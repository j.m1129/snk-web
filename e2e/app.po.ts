import { browser, element, by } from 'protractor';

export class SnkWebPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('snk-root h1')).getText();
  }
}
