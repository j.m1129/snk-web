import { SnkWebPage } from './app.po';

describe('snk-web App', () => {
  let page: SnkWebPage;

  beforeEach(() => {
    page = new SnkWebPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('snk works!');
  });
});
