import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnimeComponent } from './anime.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { ViewEpisodeComponent } from './view-episode.component';

const routes: Routes = [
{
  path: '',
  component: AnimeComponent
},
{
  path: ':temp',
  children: [
    {
      path: ':cap',
      component: ViewEpisodeComponent
    }
  ]
}
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes),
    CommonModule
  ],
  declarations: [AnimeComponent, ViewEpisodeComponent]
})
export class AnimeModule { }
